/*
	libnbds
	Copyright (C) 2014 Paweł Dziepak

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ATOMIC_H
#define ATOMIC_H

#define compare_and_swap(ptr, old, new)	\
	__sync_bool_compare_and_swap(ptr, old, new)

#define atomic_fetch_add(ptr, val)	__sync_fetch_and_add(ptr, val)
#define atomic_fetch_clear(ptr) __sync_fetch_and_and(ptr, 0)

#define atomic_or(ptr, val)	__sync_fetch_and_or(ptr, val)

#endif
