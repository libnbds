/*
	libnbds
	Copyright (C) 2014 Paweł Dziepak

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mpsc_queue.h"

#include "atomic.h"
#include "barriers.h"

#define BLOCK_SIZE	64
#define NEXT_BLOCK	(1u << 31)

struct mpsc_queue_block {
	struct mpsc_queue_block*	next;
	uint32_t					consumer;
	uint32_t					producer_producer;
};

static struct mpsc_queue_block* new_block()
{
	struct mpsc_queue_block* block;
	size_t block_size
		= sizeof(struct mpsc_queue_block) + BLOCK_SIZE * sizeof(void*);

	int retval;

	retval = posix_memalign((void**)&block, sizeof(void*), block_size);
	if (retval)
		return NULL;
	uintptr_t* data = (uintptr_t*)(block + 1);

	block->consumer = 0;
	block->producer_producer = 0;
	block->next = NULL;

	for (int i = 0; i < BLOCK_SIZE; i++)
		data[i] = (i << 1) | 1;

	return block;
}

static void lock_blocks(struct mpsc_queue* queue)
{
	atomic_fetch_add(&queue->reference_counter, 1);
}

static void unlock_blocks(struct mpsc_queue* queue)
{
	struct mpsc_queue_block* next;
	struct mpsc_queue_block* current;
	struct mpsc_queue_block* block = atomic_fetch_clear(&queue->delete_list);

	if (atomic_fetch_add(&queue->reference_counter, -1) != 1) {
		if (block) {
			current = block;
			while (current->next)
				current = current->next;

			do {
				next = queue->delete_list;
				current->next = next;
			} while (!compare_and_swap(&queue->delete_list, next, block));
		}

		return;
	}

	while (block) {
		next = block->next;
		free(block);
		block = next;
	}
}

static void free_block(struct mpsc_queue* queue, struct mpsc_queue_block* block)
{
	struct mpsc_queue_block* next;

	do {
		if (queue->reference_counter == 0) {
			free(block);
			return;
		}

		next = queue->delete_list;
		block->next = next;
	} while (!compare_and_swap(&queue->delete_list, next, block));
}

int mpsc_queue_init(struct mpsc_queue* queue)
{
	struct mpsc_queue_block* block = new_block();
	if (!block) {
		errno = ENOMEM;
		return -1;
	}

	queue->consumer = block;
	queue->producer_producer = block;
	queue->reference_counter = 0;
	queue->delete_list = NULL;

	return 0;
}

void mpsc_queue_destroy(struct mpsc_queue* queue)
{
	struct mpsc_queue_block* block;
	struct mpsc_queue_block* next;

	assert(!queue->reference_counter);

	block = queue->consumer;
	while (block) {
		next = block->next;
		free(block);
		block = next;
	}

	block = queue->delete_list;
	while (block) {
		next = block->next;
		free(block);
		block = next;
	}

	queue->consumer = NULL;
	queue->producer_producer = NULL;
	queue->delete_list = NULL;
}

int mpsc_queue_enqueue(struct mpsc_queue* queue, void* object)
{
	struct mpsc_queue_block* block;
	struct mpsc_queue_block* next_block;
	void** array;
	void** ptr;
	uint32_t producer, new_producer;
	uintptr_t value;
	int success;

	lock_blocks(queue);

	do {
		do {
			block = queue->producer_producer;

			producer = *(volatile uint32_t*)&block->producer_producer;
			if (producer & NEXT_BLOCK) {
				compare_and_swap(&queue->producer_producer, block,
					block->next);
				continue;
			}

			if (producer - block->consumer >= BLOCK_SIZE) {
				next_block = new_block();
				if (!next_block) {
					errno = ENOMEM;
					unlock_blocks(queue);
					return -1;
				}

				if (!compare_and_swap(&queue->producer_producer, block,
						next_block)) {
					free(next_block);
				} else {
					assert(!block->next);
					block->next = next_block;
					atomic_or(&block->producer_producer, NEXT_BLOCK);
				}
			} else
				break;
		} while (1);

		array = (void**)&block[1];
		ptr = &array[producer & (BLOCK_SIZE - 1)];

		value = (uintptr_t)object;
		value <<= 1;
		success = compare_and_swap(ptr, (producer << 1) | 1, value);

		new_producer = (producer + 1) & ~NEXT_BLOCK;
		compare_and_swap(&block->producer_producer, producer, new_producer);

		new_producer = *(volatile uint32_t*)&block->producer_producer;
		if (success && new_producer != (producer | NEXT_BLOCK))
			break;
	} while (1);

	unlock_blocks(queue);

	return 0;
}

void* mpsc_queue_dequeue(struct mpsc_queue* queue)
{
	struct mpsc_queue_block* block;
	struct mpsc_queue_block* next;
	void** array;
	void** ptr;
	uint32_t producer;
	uint32_t next_block;
	void* object;
	uintptr_t value;

	block = queue->consumer;

	producer = block->producer_producer;
	read_barrier();
	next = block->next;

	next_block = producer & NEXT_BLOCK;
	producer &= ~NEXT_BLOCK;

	if (producer == block->consumer && !next_block)
		return NULL;

	if (producer == block->consumer && next_block) {
		queue->consumer = next;
		free_block(queue, block);

		block = queue->consumer;
		if (block->producer_producer == block->consumer)
			return NULL;
	}

	array = (void**)&block[1];
	ptr = &array[block->consumer & (BLOCK_SIZE - 1)];
	value = (uintptr_t)*ptr;

	assert(!(value & 1));
	object = (void*)(value >> 1);

	*ptr = (void*)(uintptr_t)(((block->consumer + BLOCK_SIZE) << 1) | 1);
	write_barrier();
	block->consumer = (block->consumer + 1) & ~NEXT_BLOCK;

	return object;
}
