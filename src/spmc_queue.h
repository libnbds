/*
	libnbds
	Copyright (C) 2014 Paweł Dziepak

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NBDS_SPMC_QUEUE_H
#define NBDS_SPMC_QUEUE_H

struct spmc_queue_block;

struct spmc_queue {
	struct spmc_queue_block*	consumer_consumer;
	struct spmc_queue_block*	producer;

	uint32_t					reference_counter;
	struct spmc_queue_block*	delete_list;
};

int spmc_queue_init(struct spmc_queue* queue);
void spmc_queue_destroy(struct spmc_queue* queue);

int spmc_queue_enqueue(struct spmc_queue* queue, void* object);
void* spmc_queue_dequeue(struct spmc_queue* queue);

#endif
