/*
	libnbds
	Copyright (C) 2013 Paweł Dziepak

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NBDS_SPSC_QUEUE_H
#define NBDS_SPSC_QUEUE_H

struct spsc_queue_block;

struct spsc_queue {
	struct spsc_queue_block*	consumer;
	struct spsc_queue_block*	producer;
};

int spsc_queue_init(struct spsc_queue* queue);
void spsc_queue_destroy(struct spsc_queue* queue);

int spsc_queue_enqueue(struct spsc_queue* queue, void* object);
void* spsc_queue_dequeue(struct spsc_queue* queue);

#endif
