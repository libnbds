/*
	libnbds
	Copyright (C) 2014 Paweł Dziepak

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NBDS_MPSC_QUEUE_H
#define NBDS_MPSC_QUEUE_H

struct mpsc_queue_block;

struct mpsc_queue {
	struct mpsc_queue_block*	consumer;
	struct mpsc_queue_block*	producer_producer;

	uint32_t					reference_counter;
	struct mpsc_queue_block*	delete_list;
};

int mpsc_queue_init(struct mpsc_queue* queue);
void mpsc_queue_destroy(struct mpsc_queue* queue);

int mpsc_queue_enqueue(struct mpsc_queue* queue, void* object);
void* mpsc_queue_dequeue(struct mpsc_queue* queue);

#endif
