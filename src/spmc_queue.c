/*
	libnbds
	Copyright (C) 2014 Paweł Dziepak

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "spmc_queue.h"

#include "atomic.h"
#include "barriers.h"

#define BLOCK_SIZE	64

struct spmc_queue_block {
	struct spmc_queue_block*	next;
	uint32_t					consumer_consumer;
	uint32_t					producer;
};

static struct spmc_queue_block* new_block()
{
	struct spmc_queue_block* block;
	size_t block_size
		= sizeof(struct spmc_queue_block) + BLOCK_SIZE * sizeof(void*);
	int retval;

	retval = posix_memalign((void**)&block, sizeof(void*), block_size);
	if (retval)
		return NULL;

	memset(block, 0, block_size);
	block->consumer_consumer = 0;
	block->producer = 0;
	block->next = NULL;

	return block;
}

static void lock_blocks(struct spmc_queue* queue)
{
	atomic_fetch_add(&queue->reference_counter, 1);
}

static void unlock_blocks(struct spmc_queue* queue)
{
	struct spmc_queue_block* next;
	struct spmc_queue_block* current;
	struct spmc_queue_block* block = atomic_fetch_clear(&queue->delete_list);

	if (atomic_fetch_add(&queue->reference_counter, -1) != 1) {
		if (block) {
			current = block;
			while (current->next)
				current = current->next;

			do {
				next = queue->delete_list;
				current->next = next;
			} while (!compare_and_swap(&queue->delete_list, next, block));
		}

		return;
	}

	while (block) {
		next = block->next;
		free(block);
		block = next;
	}
}

static void free_block(struct spmc_queue* queue, struct spmc_queue_block* block)
{
	struct spmc_queue_block* next;

	do {
		next = queue->delete_list;
		block->next = next;
	} while (!compare_and_swap(&queue->delete_list, next, block));
}

int spmc_queue_init(struct spmc_queue* queue)
{
	struct spmc_queue_block* block = new_block();
	if (!block) {
		errno = ENOMEM;
		return -1;
	}

	queue->consumer_consumer = block;
	queue->producer = block;
	queue->reference_counter = 0;
	queue->delete_list = NULL;

	return 0;
}

void spmc_queue_destroy(struct spmc_queue* queue)
{
	struct spmc_queue_block* block;
	struct spmc_queue_block* next;

	block = queue->consumer_consumer;
	while (block) {
		next = block->next;
		free(block);
		block = next;
	}

	block = queue->delete_list;
	while (block) {
		next = block->next;
		free(block);
		block = next;
	}

	queue->consumer_consumer = NULL;
	queue->producer = NULL;
	queue->delete_list = NULL;
}

int spmc_queue_enqueue(struct spmc_queue* queue, void* object)
{
	struct spmc_queue_block* block;
	void** array;
	void* obj;
	uint32_t consumer;

	block = queue->producer;
	consumer = block->consumer_consumer;

	array = (void**)&block[1];
	obj = array[block->producer & (BLOCK_SIZE - 1)];

	if (block->producer - consumer >= BLOCK_SIZE || obj) {
		block = new_block();
		if (!block) {
			errno = ENOMEM;
			return -1;
		}
	}

	array = (void**)&block[1];

	assert(!array[block->producer & (BLOCK_SIZE - 1)]);
	array[block->producer & (BLOCK_SIZE - 1)] = object;
	write_barrier();
	block->producer++;

	if (block != queue->producer) {
		write_barrier();
		queue->producer->next = block;
		queue->producer = block;
	}

	return 0;
}

void* spmc_queue_dequeue(struct spmc_queue* queue)
{
	struct spmc_queue_block* block;
	struct spmc_queue_block* next;
	void** array;
	void** ptr;
	void* object;
	uint32_t producer;
	uint32_t consumer;

	lock_blocks(queue);

	do {
		do {
			block = queue->consumer_consumer;

			consumer = block->consumer_consumer;
			read_barrier();
			next = block->next;
			read_barrier();
			producer = block->producer;

			assert(producer >= consumer);
			if (producer == consumer) {
				if (!next) {
					unlock_blocks(queue);
					return NULL;
				}

				if (compare_and_swap(&queue->consumer_consumer, block, next))
					free_block(queue, block);
			} else
				break;
		} while (1);
	} while (!compare_and_swap(&block->consumer_consumer, consumer,
			consumer + 1));

	array = (void**)&block[1];
	ptr = &array[consumer & (BLOCK_SIZE - 1)];

	object = *ptr;
	assert(object);
	*ptr = NULL;

	unlock_blocks(queue);

	return object;
}
