/*
	libnbds
	Copyright (C) 2013 Paweł Dziepak

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "spsc_queue.h"

#include "barriers.h"

#define BLOCK_SIZE	64

struct spsc_queue_block {
	struct spsc_queue_block*	next;
	uint32_t					consumer;
	uint32_t					producer;
};

static struct spsc_queue_block* new_block()
{
	struct spsc_queue_block* block;
	size_t block_size
		= sizeof(struct spsc_queue_block) + BLOCK_SIZE * sizeof(void*);
	int retval;

	retval = posix_memalign((void**)&block, sizeof(void*), block_size);
	if (retval)
		return NULL;

	block->consumer = 0;
	block->producer = 0;
	block->next = NULL;

	return block;
}

int spsc_queue_init(struct spsc_queue* queue)
{
	struct spsc_queue_block* block = new_block();
	if (!block) {
		errno = ENOMEM;
		return -1;
	}

	queue->consumer = block;
	queue->producer = block;

	return 0;
}

void spsc_queue_destroy(struct spsc_queue* queue)
{
	struct spsc_queue_block* block = queue->consumer;
	struct spsc_queue_block* next;

	while (block) {
		next = block->next;
		free(block);
		block = next;
	}

	queue->consumer = NULL;
	queue->producer = NULL;
}

int spsc_queue_enqueue(struct spsc_queue* queue, void* object)
{
	struct spsc_queue_block* block;
	void** array;
	uint32_t consumer;

	block = queue->producer;
	consumer = block->consumer;

	if (block->producer - consumer >= BLOCK_SIZE) {
		block = new_block();
		if (!block) {
			errno = ENOMEM;
			return -1;
		}
	}

	array = (void**)&block[1];

	array[block->producer & (BLOCK_SIZE - 1)] = object;
	write_barrier();
	block->producer++;

	if (block != queue->producer) {
		write_barrier();
		queue->producer->next = block;
		queue->producer = block;
	}

	return 0;
}

void* spsc_queue_dequeue(struct spsc_queue* queue)
{
	struct spsc_queue_block* block;
	struct spsc_queue_block* next;
	void** array;
	void* object;
	uint32_t producer;

	block = queue->consumer;

	next = block->next;
	read_barrier();
	producer = block->producer;

	if (producer == block->consumer && !next)
		return NULL;

	if (producer == block->consumer) {
		queue->consumer = next;
		free(block);
		block = queue->consumer;
	}

	array = (void**)&block[1];
	object = array[block->consumer & (BLOCK_SIZE - 1)];
	block->consumer++;

	return object;
}
