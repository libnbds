/*
	libnbds
	Copyright (C) 2014 Paweł Dziepak

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <time.h>

#include "spmc_queue.h"

struct spmc_queue queue;

pthread_barrier_t start;

#define THREADS		4u
#define ITERATIONS	(1024 * 1024 * 8u)

struct thread_data {
	uintptr_t	read_data[ITERATIONS];
	size_t		pointer;
};
struct thread_data thread_datas[THREADS];
uintptr_t already_read;

void* thread_func(void* ptr)
{
	uintptr_t idx = *(uintptr_t*)ptr;
	struct thread_data* data = thread_datas + idx;
	uintptr_t read_count = 0;
	uintptr_t val;

	pthread_barrier_wait(&start);

	while (1) {
		do {
			val = (uintptr_t)spmc_queue_dequeue(&queue);

			if (!val) {
				if (read_count) {
					__sync_fetch_and_add(&already_read, read_count);
					read_count = 0;
				}

				if (*(volatile uintptr_t*)&already_read == ITERATIONS)
					return NULL;
			}
		} while (!val);

		if (data->pointer && data->read_data[data->pointer - 1] >= val)
			exit(1);

		data->read_data[data->pointer++] = val;
		read_count++;
	}

	return NULL;
}

int main(int argc, char** argv)
{
	uintptr_t i, j;
	int error;
	uintptr_t ids[THREADS];
	pthread_t threads[THREADS];
	uintptr_t current[THREADS];

	(void)argc;
	(void)argv;

	for (i = 0; i < THREADS; i++)
		ids[i] = i;

	error = spmc_queue_init(&queue);
	if (error)
		return 1;

	if (spmc_queue_dequeue(&queue))
		return 1;

	pthread_barrier_init(&start, NULL, THREADS + 1);

	for (i = 0; i < THREADS; i++)
		pthread_create(&threads[i], NULL, thread_func, &ids[i]);

	pthread_barrier_wait(&start);

	for (i = 1; i <= ITERATIONS; i++) {
		error = spmc_queue_enqueue(&queue, (void*)i);
		if (error)
			return 1;
	}

	for (i = 0; i < THREADS; i++)
		pthread_join(threads[i], NULL);

	if (spmc_queue_dequeue(&queue))
		return 1;

	spmc_queue_destroy(&queue);

	memset(current, 0, sizeof(current));
	for (i = 1; i <= ITERATIONS; i++) {
		error = 0;
		for (j = 0; j < THREADS; j++) {
			if (thread_datas[j].pointer == current[j])
				continue;

			if (thread_datas[j].read_data[current[j]] == i) {
				if (!error) {
					current[j]++;
					error = 1;
				} else
					return 1;
			}
		}
	}

	for (i = 0; i < THREADS; i++) {
		if (thread_datas[i].pointer != current[i])
			return 1;
	}
}
