/*
	libnbds
	Copyright (C) 2013 Paweł Dziepak

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdint.h>

#include "spsc_queue.h"

struct spsc_queue queue;

int main(int argc, char** argv)
{
	uintptr_t i;
	int error;

	(void)argc;
	(void)argv;

	error = spsc_queue_init(&queue);
	if (error)
		return 1;

	if (spsc_queue_dequeue(&queue))
		return 1;

	for (i = 1; i < 1024u; i++) {
		error = spsc_queue_enqueue(&queue, (void*)i);
		if (error)
			return 1;
	}

	for (i = 1; i < 1024u; i++) {
		if ((uintptr_t)spsc_queue_dequeue(&queue) != i)
			return 1;
	}

	if (spsc_queue_dequeue(&queue))
		return 1;

	for (i = 1; i < 1024u; i++) {
		spsc_queue_enqueue(&queue, (void*)i);
		if ((uintptr_t)spsc_queue_dequeue(&queue) != i)
			return 1;
	}

	if (spsc_queue_dequeue(&queue))
		return 1;

	spsc_queue_destroy(&queue);
	error = spsc_queue_init(&queue);
	if (error)
		return 1;

	for (i = 1; i < 1024u; i++) {
		error = spsc_queue_enqueue(&queue, (void*)i);
		if (error)
			return 1;
	}

	spsc_queue_destroy(&queue);
	error = spsc_queue_init(&queue);
	if (error)
		return 1;

	if (spsc_queue_dequeue(&queue))
		return 1;

	spsc_queue_destroy(&queue);
}
