/*
	libnbds
	Copyright (C) 2013 Paweł Dziepak

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdint.h>
#include <stdlib.h>

#include <pthread.h>

#include "spsc_queue.h"

struct spsc_queue queue;

pthread_barrier_t start;

#define ITERATIONS	(1024 * 1024 * 8u)

void* thread_func(void* ptr)
{
	uintptr_t i;

	pthread_barrier_wait(&start);

	for (i = 1; i < ITERATIONS; i++) {
		do {
			ptr = spsc_queue_dequeue(&queue);
		} while (!ptr);

		if ((uintptr_t)ptr != i)
			exit(1);
	}

	return NULL;
}

int main(int argc, char** argv)
{
	uintptr_t i;
	int error;
	pthread_t thread;

	(void)argc;
	(void)argv;

	error = spsc_queue_init(&queue);
	if (error)
		return 1;

	if (spsc_queue_dequeue(&queue))
		return 1;

	pthread_barrier_init(&start, NULL, 2);
	pthread_create(&thread, NULL, thread_func, NULL);

	pthread_barrier_wait(&start);
	for (i = 1; i < ITERATIONS; i++) {
		error = spsc_queue_enqueue(&queue, (void*)i);
		if (error)
			return 1;
	}

	pthread_join(thread, NULL);

	if (spsc_queue_dequeue(&queue))
		return 1;

	spsc_queue_destroy(&queue);
}
