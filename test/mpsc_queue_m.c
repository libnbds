/*
	libnbds
	Copyright (C) 2014 Paweł Dziepak

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <time.h>

#include "mpsc_queue.h"

struct mpsc_queue queue;

pthread_barrier_t start;

#define THREADS		4u
#define ITERATIONS	(1024 * 1024 * 2u)

void* thread_func(void* ptr)
{
	uintptr_t offset = *(uintptr_t*)ptr;
	uintptr_t i;
	int error;

	pthread_barrier_wait(&start);

	for (i = 1; i < ITERATIONS; i++) {
		error = mpsc_queue_enqueue(&queue, (void*)(i + offset));
		if (error) {
			printf("Failed to enqueue a value %#" PRIxPTR ": %s (%d)\n",
				i + offset, strerror(error), error);
			exit(1);
		}
	}

	return NULL;
}

int main(int argc, char** argv)
{
	uintptr_t i, val, idx;
	int error;
	pthread_t threads[THREADS];
	uintptr_t offsets[THREADS]
		= { 0x00000000, 0x10000000, 0x20000000, 0x30000000 };
	uintptr_t last_val[THREADS];

	(void)argc;
	(void)argv;

	memcpy(last_val, offsets, sizeof(last_val));

	error = mpsc_queue_init(&queue);
	if (error) {
		printf("Failed to init queue: %s (%d)\n", strerror(error), error);
		return 1;
	}

	val = (uintptr_t)mpsc_queue_dequeue(&queue);
	if (val) {
		printf("Dequeued a value %#" PRIxPTR "from empty queue. (after init)\n",
			val);
		return 1;
	}

	pthread_barrier_init(&start, NULL, THREADS + 1);

	for (i = 0; i < THREADS; i++)
		pthread_create(&threads[i], NULL, thread_func, &offsets[i]);

	pthread_barrier_wait(&start);

	for (i = 1; i <= (ITERATIONS - 1) * THREADS; i++) {
		do {
			val = (uintptr_t)mpsc_queue_dequeue(&queue);
		} while (!val);

		idx = val >> 28;
		if (idx >= THREADS) {
			printf("Received invalid value %#" PRIxPTR " (index: %" PRIuPTR
				", max: %u)\n", val, idx, THREADS);
			return 1;
		}
		if (last_val[idx] + 1 != val) {
			printf("Received invalid value %#" PRIxPTR ", expected: %#" PRIxPTR
				" (index: %" PRIuPTR ", max: %u)\n", val, last_val[idx] + 1,
				idx, THREADS);
			return 1;
		}
		last_val[idx]++;
	}

	for (i = 0; i < THREADS; i++)
		pthread_join(threads[i], NULL);

	val = (uintptr_t)mpsc_queue_dequeue(&queue);
	if (val) {
		printf("Dequeued a value %#" PRIxPTR "from empty queue. (after test)\n",
			val);
		return 1;
	}

	mpsc_queue_destroy(&queue);
}
